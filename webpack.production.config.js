const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: {
        "hello-world": "./src/hello-world.js",
        "my-image": "./src/myImagePage.js",
    },
    output: {
        filename: "[name].[contenthash].js", // [name] will be based on entry properties(hello-world, my-image)
        path: path.resolve(__dirname, "./dist"),
        publicPath: "auto",
        //clean needs webpack version is greater than 5.20
        // clean: {
        //     dry: true,
        //     keep: /\.css/  // only keep css files.
        // }
    },
    mode: "production",
    optimization: {
        splitChunks: {
            chunks: "all", // this is to handle external libraries e.g lodash, bundles which import dependencies, will be injected them into the html files.
            minSize: 3000, // 3kb
        },
    },
    module: {
        rules: [
            {
                test: /\.(png|jpg)$/,
                type: "asset",
                parser: {
                    dataUrlCondition: {
                        maxSize: 3 * 1024, // 3kb
                    },
                },
            },
            {
                test: /\.txt$/,
                type: "asset/source",
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, "css-loader"],
            },
            {
                test: /\.scss$/,
                use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/env"],
                        plugins: ["@babel/plugin-proposal-class-properties"],
                    },
                },
            },
            {
                test: /\.hbs$/,
                use: ["handlebars-loader"],
            },
        ],
    },
    plugins: [
        // TerserPlugin is implemented in production
        new MiniCssExtractPlugin({
            filename: "[name].[contenthash].css",
        }),
        new CleanWebpackPlugin(),
        // {
        //     cleanOnceBeforeBuildPatterns:[
        //         '**/*', //by default, remove all files in output path, this case is the dist folder
        //         path.join(process.cwd(), 'build/**/*') // remove all files outside the output path. All files in build folder are removed
        //     ]
        // }
        new HtmlWebpackPlugin({
            filename: "hello-world.html",
            chunks: ["hello-world"], // will be based on entry properties(hello-world)
            title: "Hello world",
            template: "src/page-template.hbs",
            description: "Hello World",
            minify: false, // default is true
        }),
        new HtmlWebpackPlugin({
            filename: "my-image.html",
            chunks: ["my-image"], //will be based on entry properties(my-image)
            title: "My image",
            template: "src/page-template.hbs",
            description: "My Image",
            minify: false, // default is true
        }),
    ],
};
