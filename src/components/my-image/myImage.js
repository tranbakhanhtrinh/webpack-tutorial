import Me from './a.jpg'
import './myImage.scss'

class MyImage {
    render(){
        const img = document.createElement('img');
        img.src = Me;
        img.alt = "My image";
        img.classList.add("my-image");

        const body = document.querySelector("body")
        body.appendChild(img)
    }
}

export default MyImage;