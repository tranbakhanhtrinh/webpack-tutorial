const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: {
        'hello-world': './src/hello-world.js',
        'my-image': './src/myImagePage.js',
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname,'./dist'),
        publicPath: 'auto',
        //clean needs webpack version is greater than 5.20
        // clean: {
        //     dry: true, 
        //     keep: /\.css/  // only keep css files. 
        // } 
    },
    mode: "development",
    devServer: {
        port: 9000,
        static: {
            directory:path.resolve(__dirname,'./dist')
        },
        devMiddleware: {
            index: 'index.html',
            writeToDisk: true
        }
    },
    module: {
        rules: [
            {
                test: /\.(png|jpg)$/,
                type: "asset",
                parser: {
                    dataUrlCondition: {
                        maxSize: 3 * 1024 // 3kb
                    }
                }
            },
            {
                test: /\.txt$/,
                type: "asset/source",
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader', 'css-loader'
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader', 'css-loader', 'sass-loader'
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets:['@babel/env'],
                        plugins: ['@babel/plugin-proposal-class-properties']
                    }
                }
            },
            {
                test: /\.hbs$/,
                use: [
                    'handlebars-loader'
                ]
            }
        ]
    },
    plugins:[
        new CleanWebpackPlugin(
            // {
            //     cleanOnceBeforeBuildPatterns:[
            //         '**/*', //by default, remove all files in output path, this case is the dist folder
            //         path.join(process.cwd(), 'build/**/*') // remove all files outside the output path. All files in build folder are removed
            //     ]
            // }
        ),
        new HtmlWebpackPlugin({
            filename:  'hello-world.html',
            chunks: ['hello-world'],
            title: 'Hello world',
            template:'src/page-template.hbs',
            description: "Hello World"
        }),
        new HtmlWebpackPlugin({
            filename:  'my-image.html',
            chunks: ['my-image'],
            title: 'My image',
            template:'src/page-template.hbs',
            description: "My image"
        })

    ]
}