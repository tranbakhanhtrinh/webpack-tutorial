import Heading from "./components/heading/heading";
import MyImage from "./components/my-image/myImage";
import React from 'react'

const heading = new Heading();
heading.render("my image");

const image = new MyImage();
image.render();
